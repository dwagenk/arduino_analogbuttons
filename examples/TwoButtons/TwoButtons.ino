/*
 This is a sample sketch to show how to use the AnalogButtonsLibrary
 to detect click events on 2 buttons in parallel. 

 Setup a test circuit:
 * Create a resistor network of 3 resistors between GND
   and +5V. Overall resistance should be at least 5kOhm
 * Connect one pushbutton between R1 and R2, with the
   second site attached to Arduino Analog Input A0
 * Connect a second pushbutton between R2 and R3, with the
   second site attached to Arduino Analog Input A0

   GND___[R1]____[R2]____[R3]___+5V
              |       |
              \-PBa   \-PBb
              |_______|
                  |
                  A0

 * Run the Arduino Sample Basic->AnalogReadSerial with the
   following added line to the setup() function:
      pinMode(A0, INPUT_PULLUP);
 * Note down the analog read values given on the serial console for:
      * PBa pressed
      * PBb pressed
      * PBa and PBb pressed at the same time
 * Fill those values in for the buttonLevels array below
 * The Serial interface is used to output the detected button events.

*/

// 01.03.2014 created by Matthias Hertel
// ... and working.
// 21.09.2019 Daniel Wagenkencht: adapted to work with AnalogButtons instead
//            of OneButton library

/* Sample output:

Starting AnalogButtons...
Button click           : 0 -- PBa
Button click           : 1 -- PBb
Button click           : 2 -- PBa and PBb
Button doubleclick     : 0
Button doubleclick     : 1
Button doubleclick     : 2
Button longPress start : 0
Button longPress ...   : 0
Button longPress stop  : 0
Button longPress start : 1
Button longPress ...   : 1
Button longPress ...   : 1
Button longPress stop  : 1
Button longPress start : 2
Button longPress ...   : 2
Button longPress stop  : 2

*/

#include "AnalogButtons.h"

// Select the pin and pullup configuration
#define ANALOG_PIN        A0
#define PULLUP_ACTIVE     true

// Set up the list of buttons
unsigned int buttonLevels[] = {
  702,  // 0: PBa
  377,  // 1: PBb
  535   // 2: PBa and PBb
        //    ^button combinations need to be listed last
};
#define NUM_BUTTON_LEVELS 3

// Analog values might vary a little.
// Treat   value +-20   as valid
#define ANALOG_PADDING    20 

// Setup the AnalogButtons library 
AnalogButtons buttons(ANALOG_PIN,
                      PULLUP_ACTIVE,
                      buttonLevels,
                      NUM_BUTTON_LEVELS,
                      ANALOG_PADDING);

// setup code here, to run once:
void setup() {
  // Setup the Serial port. see http://arduino.cc/en/Serial/IfSerial
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for Leonardo only
  }
  Serial.println("Starting AnalogButtons...");

  // link the button functions.
  buttons.attachClick(click);
  buttons.attachDoubleClick(doubleclick);
  buttons.attachLongPressStart(longPressStart);
  buttons.attachLongPressStop(longPressStop);
  buttons.attachDuringLongPress(longPress);

  buttons.setDebounceTicks(25);
  buttons.setClickTicks(300);
  buttons.setPressTicks(500);
} // setup


// main code here, to run repeatedly: 
void loop() {
  // keep watching the push buttons:
  buttons.tick();

  // You can implement other code in here or just wait a while 
  delay(10);
} // loop


// ----- buttons callback functions

// This function will be called when a button was pressed 1 time (and no 2. button press followed).
void click(unsigned int button) {
  Serial.print("Button click           : ");
  Serial.print(button);
  if (button == 0) {
    Serial.print(" -- PBa");
  } else if (button == 1) {
    Serial.print(" -- PBb");
  } else if (button == 2) {
    Serial.print(" -- PBa and PBb");
  }
  Serial.println();
} // click


// This function will be called when a button was pressed 2 times in a short timeframe.
void doubleclick(unsigned int button) {
  Serial.print("Button doubleclick     : ");
  Serial.println(button);
} // doubleclick


// This function will be called once, when a button is pressed for a long time.
void longPressStart(unsigned int button) {
  Serial.print("Button longPress start : ");
  Serial.println(button);
} // longPressStart


// This function will be called often, while a button is pressed for a long time.
void longPress(unsigned int button) {
  Serial.print("Button longPress ...   : ");
  Serial.println(button);
} // longPress


// This function will be called once, when a button is released after beeing pressed for a long time.
void longPressStop(unsigned int button) {
  Serial.print("Button longPress stop  : ");
  Serial.println(button);
} // longPressStop

// End

Arduino AnalogButtons Library
===

This Arduino library allows using multiple buttons on one analog port supporting various types of button-events.
It shows how to use an analog input pin with multiple pushbuttons attached, each causing a different voltage
level on the pin. It supports detecting some of the typical button press events like single clicks, double
clicks and long-time pressing.

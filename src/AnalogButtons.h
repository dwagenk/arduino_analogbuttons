// -----
// AnalogButtons.h - Library for detecting button clicks, doubleclicks and long
// press pattern on a single button. This class is implemented for use with the
// Arduino environment.
// Copyright (c) by Daniel Wagenknecht
// Based on previous work published and licensed by
// Copyright (c) by Matthias Hertel,
// http://www.mathertel.de This work is licensed under a BSD style license. See
// http://www.mathertel.de/License.aspx More information on:
// http://www.mathertel.de/Arduino
// -----
// 02.10.2010 created by Matthias Hertel
// 21.04.2011 transformed into a library
// 01.12.2011 include file changed to work with the Arduino 1.0 environment
// 23.03.2014 Enhanced long press functionalities by adding longPressStart and
// longPressStop callbacks
// 21.09.2015 A simple way for debounce detection added.
// 14.05.2017 Debouncing improvements.
// 25.06.2018 Optional third parameter for deactivating pullup.
// 26.09.2018 Anatoli Arkhipenko: Included solution to use library with other
// sources of input.
// 26.09.2018 Initialization moved into class declaration.
// 26.09.2018 Jay M Ericsson: compiler warnings removed.
// 19.09.2019 Daniel Wagenknecht: Fork, rename and adapt library to work on
// multiple buttons connected to an analog port.
// -----

#ifndef AnalogButtons_h
#define AnalogButtons_h

#include "Arduino.h"

// ----- Callback function types -----

extern "C" {
typedef void (*callbackFunction)(unsigned int);
}

class AnalogButtons
{
public:
  // ----- Constructor -----
  AnalogButtons();

  AnalogButtons(int pin,
                bool pullupActive,
                unsigned int* buttonLevelsArray,
                unsigned int numButtonLevels,
                unsigned int analogPadding);

  // ----- Set runtime parameters -----

  // set # millisec after safe click is assumed.
  void setDebounceTicks(int ticks);

  // set # millisec after single click is assumed.
  void setClickTicks(int ticks);

  // set # millisec after press is assumed.
  void setPressTicks(int ticks);

  // attach functions that will be called when button was pressed in the
  // specified way.
  void attachClick(callbackFunction newFunction);
  void attachDoubleClick(callbackFunction newFunction);
  void attachLongPressStart(callbackFunction newFunction);
  void attachLongPressStop(callbackFunction newFunction);
  void attachDuringLongPress(callbackFunction newFunction);

  // ----- State machine functions -----

  /**
   * @brief Call this function every few milliseconds for checking the input
   * level at the initialized analog pin.
   */
  void tick(void);

  /**
   * @brief Call this function every time the input level has changed.
   * Using this function the analog input will not be read because the current
   * level is given by the parameter.
   */
  void tick(int analogLevel);

  bool isLongPressed();
  int getPressedTicks();
  void reset(void);

private:
  int _pin; // hardware pin number.
  unsigned int* _buttonLevelsArray; //Pointer to list of analog values
                                    //corresponding to different buttons
  unsigned int _numButtonLevels;    //Number of entries in above list
  unsigned int _analogPadding;      //Range around every analog value given above, that
                                    //is considered valid

  unsigned int _debounceTicks = 50; // number of ticks for debounce times.
  unsigned int _clickTicks = 600;   // number of ticks that have to pass by
                                    // before a click is detected.
  unsigned int _pressTicks = 1000;  // number of ticks that have to pass by
                                    // before a long button press is detected

  bool _isLongPressed = false;
  int _currentButtonId;

  // These variables will hold functions acting as event source.
  callbackFunction _clickFunc = NULL;
  callbackFunction _doubleClickFunc = NULL;
  callbackFunction _pressFunc = NULL;
  callbackFunction _longPressStartFunc = NULL;
  callbackFunction _longPressStopFunc = NULL;
  callbackFunction _duringLongPressFunc = NULL;

  // These variables hold information across the upcoming tick calls.
  // They are initialized once on program start and are updated every time the
  // tick function is called.
  enum {
    S0_IDLE,
    S1_FIRST_PRESS_STARTED,
    S2_FIRST_PRESS_RELEASED,
    S3_SECOND_PRESS_STARTED,
    S4_GRACE_TIME,
    S6_DURING_LONG_PRESS
  } _state = S0_IDLE;
  unsigned long _startTime;
  unsigned long _stopTime;

  // Map analog levels to buttons
  int getButtonId (unsigned int analogLevel);
};

#endif /* AnalogButtons_h */

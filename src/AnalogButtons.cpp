/**
 * @file AnalogButtons.cpp
 *
 * @brief This Arduino library allows using multiple buttons on one analog port
 * supporting various types of button-events.
 *
 * @author Daniel Wagenknecht, https://dwagenk.com
 * @Copyright Copyright (c) 2019 by Daniel Wagenknecht, https://dwagenk.com
 *
 * Based on previous work published and licensed by
 * @author Matthias Hertel, https://www.mathertel.de
 * @Copyright Copyright (c) by Matthias Hertel, https://www.mathertel.de.
 *
 * This work is licensed under a BSD style license. See the file "LICENSE"
 * in this repository.
 *
 * Changelog: see AnalogButtons.h
 */

#include "AnalogButtons.h"

// ----- Initialization and Default Values -----
#define BUTTONS_IDLE (-1)

/**
 * @brief Construct a new AnalogButtons object but not (yet) initialize the IO pin.
 */
AnalogButtons::AnalogButtons()
{
  _pin = -1;
}

AnalogButtons::AnalogButtons(int pin,
                bool pullupActive,
                unsigned int* buttonLevelsArray,
                unsigned int numButtonLevels,
                unsigned int analogPadding)
{
  _pin = pin;
  _buttonLevelsArray = buttonLevelsArray;
  _numButtonLevels = numButtonLevels;
  _analogPadding = analogPadding;

  if (pullupActive) {
    // Use the given pin as input and activate internal PULLUP resistor.
    pinMode(pin, INPUT_PULLUP);
  } else {
    // uUse the given pin as input
    pinMode(pin, INPUT);
  }
}

// Explicitly set the number of millisec that have to pass by before a click is
// assumed as safe.
void AnalogButtons::setDebounceTicks(int ticks)
{
  _debounceTicks = ticks;
}

// Explicitly set the number of millisec that have to pass by before a click is
// detected.
void AnalogButtons::setClickTicks(int ticks)
{
  _clickTicks = ticks;
}

// Explicitly set the number of millisec that have to pass by before a long
// button press is detected.
void AnalogButtons::setPressTicks(int ticks)
{
  _pressTicks = ticks;
}

// save function for click event
void AnalogButtons::attachClick(callbackFunction newFunction)
{
  _clickFunc = newFunction;
}

// save function for doubleClick event
void AnalogButtons::attachDoubleClick(callbackFunction newFunction)
{
  _doubleClickFunc = newFunction;
}

// save function for longPressStart event
void AnalogButtons::attachLongPressStart(callbackFunction newFunction)
{
  _longPressStartFunc = newFunction;
}

// save function for longPressStop event
void AnalogButtons::attachLongPressStop(callbackFunction newFunction)
{
  _longPressStopFunc = newFunction;
}

// save function for during longPress event
void AnalogButtons::attachDuringLongPress(callbackFunction newFunction)
{
  _duringLongPressFunc = newFunction;
}

// get the current long pressed state
bool AnalogButtons::isLongPressed(){
  return _isLongPressed;
}

int AnalogButtons::getPressedTicks(){
  return _stopTime - _startTime;
}

void AnalogButtons::reset(void){
  _state = S0_IDLE; // restart.
  _startTime = 0;
  _stopTime = 0;
  _isLongPressed = false;
}

/**
 * @brief Translate analog level to id of button beeing pressed.
 */
int AnalogButtons::getButtonId (unsigned int analogLevel)
{
  for (unsigned int i = 0; i < _numButtonLevels; i++) {
    if (   (analogLevel >= (_buttonLevelsArray[i] - _analogPadding))
        && (analogLevel <= (_buttonLevelsArray[i] + _analogPadding)) 
       ) {
      return i;
    }
  }

  return BUTTONS_IDLE;
}

/**
 * @brief Check input of the configured pin and then advance the finite state
 * machine (FSM).
 */
void AnalogButtons::tick(void)
{
  if (_pin >= 0) {
    tick(analogRead(_pin));
  }
}

/**
 * @brief Advance the finite state machine (FSM) using the given analog level.
 */
void AnalogButtons::tick(int analogLevel)
{
  unsigned long now = millis();

  int buttonId = getButtonId(analogLevel);

  // Implementation of the state machine

  if (_state == S0_IDLE) {
    // Waiting for a button to be pressed
    if (buttonId != BUTTONS_IDLE) {
      _state = S1_FIRST_PRESS_STARTED;
      _startTime = now;
      _currentButtonId = buttonId;
    }

  } else if (_state == S1_FIRST_PRESS_STARTED) {
    // Waiting for the button to be released again

    if ((buttonId != _currentButtonId) &&
        ((unsigned long)(now - _startTime) < _debounceTicks)) {
      // Button was released to quickly so assume it's bouncing.
      // Go back to waiting state without calling a function.
      _state = S0_IDLE;

    } else if (buttonId > _currentButtonId) {
      // It's not the same button(-combination) being pressed anymore.
      // Higher buttonIds are prioritized. This allows button combinations
      // to be used as well, as long as they are listed last in the
      // buttonLevelsArray.
      // Go back to waiting state without calling a function.
      _state = S0_IDLE;

    } else if (buttonId == BUTTONS_IDLE ) {
      // Button was released after debouncing period.
      _state = S2_FIRST_PRESS_RELEASED;
      _stopTime = now;

    } else if ((buttonId == _currentButtonId) &&
               ((unsigned long)(now - _startTime) > _pressTicks)) {
      // Button has been pressed long enough for longpress
      _isLongPressed = true;
      if (_pressFunc)
        _pressFunc(_currentButtonId);
      if (_longPressStartFunc)
        _longPressStartFunc(_currentButtonId);
      if (_duringLongPressFunc)
        _duringLongPressFunc(_currentButtonId);
      _state = S6_DURING_LONG_PRESS;
      _stopTime = now;
    } else {
      // Wait. Stay in this state.
    }

  } else if (_state == S2_FIRST_PRESS_RELEASED) {
    // Waiting for the button to be pressed the second time or timeout.
    if (_doubleClickFunc == NULL ||
        (unsigned long)(now - _startTime) > _clickTicks) {
      // This was only a single short click
      if (_clickFunc)
        _clickFunc(_currentButtonId);
        _state = S0_IDLE;

    } else if ((buttonId == _currentButtonId) &&
               ((unsigned long)(now - _stopTime) > _debounceTicks)) {
      // The button has been pressed a second time and is properly debounced.
      _state = S3_SECOND_PRESS_STARTED;
      _startTime = now;
    } else {
      // Wait. Stay in this state.
    }

  } else if (_state == S3_SECOND_PRESS_STARTED) {
    // Waiting for the button to be released after second press.
    // Stay here for at least _debounceTicks because else we might end up in
    // evaluating another pressing, if the button bounces for too long.
    if ((buttonId != _currentButtonId) &&
        ((unsigned long)(now - _startTime) > _debounceTicks)) {
      // This was a 2 click sequence.
      if (_doubleClickFunc)
        _doubleClickFunc(_currentButtonId);
      _state = S0_IDLE;
      _stopTime = now;
    }

  } else if (_state == S6_DURING_LONG_PRESS) {
    // Waiting for the button to be released after long press.
    if (buttonId == BUTTONS_IDLE) {
      _isLongPressed = false;
      if (_longPressStopFunc)
        _longPressStopFunc(_currentButtonId);
      _state = S0_IDLE;
      _stopTime = now;
    } else {
      // Button is still being pressed
      _isLongPressed = true;
      if (_duringLongPressFunc)
        _duringLongPressFunc(_currentButtonId);
    }

  }
}


// end.
